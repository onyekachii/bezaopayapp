﻿using BezaoPayATMAppDAL.Repository.EntityRepository;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BezaoPayATMAppDAL.UnitWork
{
    public interface IUnitOfWork : IDisposable
    {
        TransactionRepository Transactions { get; }
        AccountRepository Accounts { get; }
        UserRepository Users { get; }
        Task<int> CommitAsync();
        void Commit();
    }
}
