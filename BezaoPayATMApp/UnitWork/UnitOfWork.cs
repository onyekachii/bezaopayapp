﻿using BezaoPayATMAppDAL.Context;
using BezaoPayATMAppDAL.Repository.EntityRepository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BezaoPayATMAppDAL.UnitWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DbContext _context;

        private TransactionRepository _transactions;
        private AccountRepository _accounts;
        private UserRepository _users;

        public UnitOfWork(DbContext context) => _context = context;
        public UnitOfWork() => _context = new BPContext();

        public TransactionRepository Transactions
        {
            get
            { return _transactions ??= _transactions = new TransactionRepository(_context); }
        }
        public AccountRepository Accounts
        {
            get
            { return _accounts ??= _accounts = new AccountRepository(_context); }
        }
        public UserRepository Users
        {
            get
            { return _users ??= _users = new UserRepository(_context); }
        }

        public void Commit() => _context.SaveChanges();        

        public async Task<int> CommitAsync() => await _context.SaveChangesAsync();

        public void Dispose() => _context.Dispose();
    }
}
