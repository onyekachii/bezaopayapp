﻿using BezaoPayATMAppDAL.Models.BezaoPayServiceModels;
using BezaoPayATMAppDAL.UnitWork;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace BezaoPayATMAppDAL.Repository.Services.Utilities
{
    class RegistrationTools
    {
        ConsoleColor DarkCyan = ConsoleColor.DarkCyan;
        ConsoleColor DarkRed = ConsoleColor.DarkRed;
        private readonly IUnitOfWork _unitOfWork;
      
        public RegistrationTools(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        internal bool ValidateRegistration(RegisterViewModel model)
        {
            string[] stringItems = { model.FirstName, model.LastName };
            var isStringError = IsStringError(stringItems);

            var isEmailError = IsEmailerror(model.Email);

            var isUserNameError = IsUserNameError(model.Username);

            var isPinError = IsPinError(model.Passcode, model.ConfirmPasscode);

            if (isStringError || isEmailError || isUserNameError || isPinError)
                return false;
            return true;
        }

        internal double AccountNumberGenerator()
        {
            var x = DateTime.Now;
            var sec = Convert.ToString(x.Second);
            var min = Convert.ToString(x.Minute);
            var hr = Convert.ToString(x.Hour);
            var dy = Convert.ToString(x.Day);
            var mon = Convert.ToString(x.Month);
            var yr = Convert.ToString(x.Year);

            var accountNumber = $"{sec}{min}{hr}{dy}{mon}{yr}";
            return Convert.ToDouble(accountNumber);
        }
        bool IsStringError(params string[] str)
        {
            bool Iserror = false;
                        
            foreach (var item in str)
            {
                if (string.IsNullOrWhiteSpace(item))
                {
                    Console.WriteLine("Invalid Input: Neither empty name field or whitespace is allowed");
                    return Iserror = true;
                }

                Regex stringRegex = new Regex(@"^[a-zA-Z]*$");
                Match PasscodeMatch = stringRegex.Match(item);
                if (PasscodeMatch.Success)
                {
                    Iserror = false;                    
                }
                else
                {
                    Console.ForegroundColor = DarkRed;
                    Console.WriteLine($"{item} is incorrect, " +
                    $"your input should correlate with the roman alphabet letters.");
                    Iserror = true;
                    Console.ForegroundColor = DarkCyan;
                    break;
                }
                
            }
            return Iserror;
        }

        bool IsEmailerror(string email)
        {
            var Ismail = _unitOfWork.Users.IsMailExist(email);            

            if (Ismail)
            {
                return Ismail;
            }
            try
            {
                var m = new System.Net.Mail.MailAddress(email);
                return false;
            }
            catch
            {
                Console.ForegroundColor = DarkRed;
                Console.WriteLine("Incorrect Email format");
                Console.ForegroundColor = DarkCyan;
                return true;
            }            
        }

        bool IsUserNameError(string username)
        {
            var IsUserExist = _unitOfWork.Users.IsUserNameExist(username);
            if (IsUserExist)
            {
                return IsUserExist;
            }
            return false;
        }

        bool IsPinError(string pin1, string pin2)
        {
            bool isError = false;

            if (pin1 != pin2)
            {
                Console.WriteLine("PIN and Confirm PIN has to be the same");
                return true;
            }

            Regex PasscodeRegex = new Regex(@"\D+");
            Match PasscodeMatch = PasscodeRegex.Match(pin2);
            if (PasscodeMatch.Success || pin2.Length != 4)
            {
                Console.ForegroundColor = DarkRed;
                Console.WriteLine("Please provide a valid 4-digit PIN");
                isError = true;
                Console.ForegroundColor = DarkCyan;
                return isError;
            }
            return isError;
        }
    }
}
