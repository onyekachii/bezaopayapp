﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BezaoPayATMAppDAL.Repository.Services.Utilities
{
    class InvalidPasswordException : Exception
    {        
        public InvalidPasswordException() : base(String.Format("Invalid password"))
        {

        }
    }
}
