﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BezaoPayATMAppDAL.Models.BezaoPayServiceModels;
using BezaoPayATMAppDAL.Models.Entities;
using System.Text.RegularExpressions;
using BezaoPayATMAppDAL.Repository.Services.Utilities;
using BezaoPayATMAppDAL.UnitWork;
using BezaoPayATMAppDAL.Context;
using System.Linq.Expressions;

namespace BezaoPayATMAppDAL.Repository.Services
{
    public class BezaoPayUserService
    {
        private readonly UnitOfWork _unitOfWork;
        public BezaoPayUserService(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task RegisterAsync(RegisterViewModel model)
        {
            var registrationTool = new RegistrationTools(_unitOfWork);
            if (!registrationTool.ValidateRegistration(model))
            {
                Console.WriteLine("\nRegistration error");
                return;
            }

            string pin = model.ConfirmPasscode;
            string mySalt = BCrypt.Net.BCrypt.GenerateSalt();
            string PepperDem = "TryReading@YourOwnRiskT#eseAreT#eListOfGirl$OnMyCrushList:Get@Abeg";
            string PIN = @$"{pin}{PepperDem}";
            string myHash = BCrypt.Net.BCrypt.HashPassword(PIN, mySalt);
            
            var user = new User
            {
                FirstName = model.FirstName,
                LastName = model.LastName,
                Name = $"{model.FirstName} {model.LastName}",
                Email = model.Email,
                UserName = model.Username,
                Birthday = model.Birthday,
                IsActive = true,
                Passcode = myHash,
                Salt = mySalt,
                Account = new Account { AccountNumber = registrationTool.AccountNumberGenerator(), Balance = 1_000_000, Created = DateTime.Now }
            };         

            var userDetails = new StringBuilder();
            userDetails.AppendLine($"{user.FirstName} Details");
            userDetails.AppendLine($"Name ****** Email ************* Username ****** Birthday");
            userDetails.AppendLine($"{user.FirstName}\n{user.LastName} **** {user.Email} ********* {user.UserName}***\t{user.Birthday}\n");
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine( userDetails.ToString() );
            Console.ForegroundColor = ConsoleColor.DarkCyan;
            userDetails.Clear();

            Console.WriteLine("Press 1 to confirm your detail or any other key to cancel");
            var userinput = Console.ReadLine();
            switch (userinput)
            {
                case "1":
                    await _unitOfWork.Users.AddAsync(user);
                    Console.WriteLine("Database updated");
                    _unitOfWork.Users.Save();
                    _unitOfWork.Commit();
                    Console.WriteLine($"Success! {user.FirstName} is now a customer of BPay. Yay!.");
                    break;
                default:
                    Console.WriteLine("Registeration cancelled");
                    break;
            }
        }

        public void Update(UpdateViewModel model)
        {
             
        }

        public void Login(LoginViewModel model)
        {                       
            var IsUserExist = _unitOfWork.Users.IsUserNameExist(model.Username, 1);
            var mail = _unitOfWork.Users.IsMailExist(model.Username, 1);
            if (IsUserExist || mail)
            {
                Expression<Func<User, bool>> expression = user => user.UserName == model.Username;
                var user = _unitOfWork.Users.Find(expression);
                foreach (var u in user)
                {
                    var salt = u.Salt;
                    string pin = model.Passcode;
                    string PepperDem = "TryReading@YourOwnRiskT#eseAreT#eListOfGirl$OnMyCrushList:Get@Abeg";
                    string PIN = @$"{pin}{PepperDem}";
                    string myHash = BCrypt.Net.BCrypt.HashPassword(PIN, salt);
                    if (u.Passcode != myHash)
                        throw new InvalidPasswordException();
                    Console.WriteLine($"Welcome {u.Name}");
                }                    
            }                        
            throw new InvalidPasswordException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public void Get(int id)
        {
            throw new NotImplementedException();
        }        
    }
}
