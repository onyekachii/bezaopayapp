﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace BezaoPayATMAppDAL.Repository.EntityRepository.Interfaces
{
    public interface IRepository<T> where T : class
    {
        IQueryable<T> GetAll();
        T Get(object id);
        Task<T> GetAsync(object id);
        void Add(T entity);
        Task AddAsync(T entity);
        void AddRange(IList<T> obj);
        Task AddRangeAsync(IList<T> obj);
        void Update(T obj);
        void DeleteById(object id);
        void Delete(T obj);
        void DeleteRange(IList<T> obj);

        IQueryable<T> Find(Expression<Func<T, bool>> predicate);
        void Save();
    }
}
