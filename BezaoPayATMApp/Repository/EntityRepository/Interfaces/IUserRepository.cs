﻿using BezaoPayATMAppDAL.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace BezaoPayATMAppDAL.Repository.EntityRepository.Interfaces
{
    public interface IUserRepository : IRepository<User>
    {
        public bool IsUserNameExist(string username);
        public bool IsMailExist(string email);
    }
}
