﻿using BezaoPayATMAppDAL.Models.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace BezaoPayATMAppDAL.Repository.EntityRepository.Interfaces
{
    public interface IAccountRepository : IRepository<Account>
    {
    }
}
