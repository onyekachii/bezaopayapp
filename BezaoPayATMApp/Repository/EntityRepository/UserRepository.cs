﻿using BezaoPayATMAppDAL.Context;
using BezaoPayATMAppDAL.Models.Entities;
using BezaoPayATMAppDAL.Repository.EntityRepository.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BezaoPayATMAppDAL.Repository.EntityRepository
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        private readonly DbContext _context;
        public UserRepository(DbContext context) => _context = context;
        public UserRepository() => _context = new BPContext();

        public bool IsUserNameExist(string username)
        {
            var user = _context.Set<User>().Where(u => u.UserName == username).ToList();
                        
            if (user.Any())
            {
                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.WriteLine($"{username} already exists");
                Console.ForegroundColor = ConsoleColor.DarkCyan;
                return true;
            }           
            return false;
        }

        public bool IsUserNameExist(string username, int num)
        {
            var user = _context.Set<User>().Where(u => u.UserName == username).ToList();

            if (user.Any())
            {                
                return true;
            }
            return false;
        }

        public bool IsMailExist(string email)
        {
            var mail = _context.Set<User>().Where(u => u.Email == email).ToList();

            if (mail.Any())
            {
                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.WriteLine($"{email} already exists");
                Console.ForegroundColor = ConsoleColor.DarkBlue;
                return true;
            }
            return false;
        }

        public bool IsMailExist(string email, int num)
        {
            var mail = _context.Set<User>().Where(u => u.Email == email).ToList();

            if (mail.Any())
            {                
                return true;
            }
            return false;
        }
    }
}
