﻿using BezaoPayATMAppDAL.Context;
using BezaoPayATMAppDAL.Models.Entities;
using BezaoPayATMAppDAL.Repository.EntityRepository.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace BezaoPayATMAppDAL.Repository.EntityRepository
{
    public class TransactionRepository : Repository<Transaction>, ITransactionRepository
    {
        private readonly DbContext _context;
        public TransactionRepository(DbContext context) : base(context) => _context = context;
        public TransactionRepository() => _context = new BPContext();
    }
}
