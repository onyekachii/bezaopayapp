﻿using BezaoPayATMAppDAL.Context;
using BezaoPayATMAppDAL.Repository.EntityRepository.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace BezaoPayATMAppDAL.Repository.EntityRepository
{
    public abstract class Repository<T> : IRepository<T> where T : class
    {
        private readonly DbContext _context;
        private DbSet<T> table = null;
        private bool disposed = false;

        public Repository()
        {
            _context = new BPContext();
            table = _context.Set<T>();
        }
        public Repository(DbContext context)
        {
            _context = context;
            table = _context.Set<T>();
        }
        public IQueryable<T> GetAll()
        {
            return table.AsQueryable();
        }
        public T Get(object id)
        {
            return table.Find(id);
        }
        public async Task<T> GetAsync(object id)
        {
            return await table.FindAsync(id);
        }
        public void Add(T entity)
        {
            table.Add(entity);
        }
        public async Task AddAsync(T entity)
        {
            await table.AddAsync(entity);
        }
        public void AddRange(IList<T> obj)
        {
            table.AddRange(obj);
        }
        public async Task AddRangeAsync(IList<T> obj)
        {
            await table.AddRangeAsync(obj);
        }
        public void Update(T entity)
        {
            table.Update(entity);
        }
        public void DeleteById(object id)
        {
            var item = table.Find(id);
            table.Remove(item);
        }
        public void Delete(T entity)
        {
            table.Remove(entity);
        }
        public void DeleteRange(IList<T> obj)
        {
            table.RemoveRange(obj);
        }

        public IQueryable<T> Find(Expression<Func<T, bool>> predicate)
        {
            return table.Where(predicate).AsQueryable();
        }
        public void Save()
        {
            _context.SaveChanges();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
