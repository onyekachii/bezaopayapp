﻿using BezaoPayATMAppDAL.Context;
using BezaoPayATMAppDAL.Models.Entities;
using BezaoPayATMAppDAL.Repository.EntityRepository.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace BezaoPayATMAppDAL.Repository.EntityRepository
{
    public class AccountRepository : Repository<Account>, IAccountRepository
    {
        private readonly DbContext _context;
        public AccountRepository(DbContext context) => _context = context;
        public AccountRepository() => _context = new BPContext();
    }
}
