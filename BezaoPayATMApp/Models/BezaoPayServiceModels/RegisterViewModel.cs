﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BezaoPayATMAppDAL.Models.BezaoPayServiceModels
{
    public class RegisterViewModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string Passcode { get; set; }
        public string ConfirmPasscode { get; set; }
        public DateTime Birthday { get; set; }
    }
}
