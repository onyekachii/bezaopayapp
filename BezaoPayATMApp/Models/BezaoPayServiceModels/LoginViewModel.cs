﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BezaoPayATMAppDAL.Models.BezaoPayServiceModels
{
    public class LoginViewModel
    {       
        public string Username { get; set; }
        public string Passcode { get; set; }
        
    }
}
