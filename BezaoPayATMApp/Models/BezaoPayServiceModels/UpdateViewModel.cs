﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BezaoPayATMAppDAL.Models.BezaoPayServiceModels
{
    public class UpdateViewModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string CurrentPassword { get; set; }
        public string NewPassword { get; set; }
        public string ConfirmNewPassword { get; set; }
        public DateTime Birthday { get; set; }
    }
}
