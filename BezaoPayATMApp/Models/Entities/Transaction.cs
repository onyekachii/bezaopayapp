﻿using BezaoPayATMAppDAL.Models.Entities.Enumerator;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace BezaoPayATMAppDAL.Models.Entities
{
    public class Transaction
    {
        public int Id { get; set; }

        public int UserId { get; set; }

        public User User { get; set; }

        public TransactionMode TransactionMode { get; set; }

        [Column(TypeName = "decimal(38,2)")]
        public decimal Amount { get; set; }

        public DateTime TimeStamp { get; set; }
    }
}
