﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BezaoPayATMAppDAL.Models.Entities
{
    public class User
    {
        private string _passcode;
        private string _salt;

        public int Id { get; set; }

        public int AccountId { get; set; }
        public Account Account { get; set; }

        [MaxLength(50)]
        [MinLength(4)]
        public string FirstName { get; set; }

        [MaxLength(50)]
        [MinLength(4)]
        public string LastName { get; set; }

        public string Name { get; set; }

        [MaxLength(50)]
        [MinLength(4)]
        public string UserName { get; set; }

        [MaxLength(50)]
        public string Email { get; set; }

        public string Passcode { get => _passcode; set => _passcode = value; }

        public string Salt { get => _salt; set => _salt = value; }

        public DateTime Birthday { get; set; }

        public bool IsActive { get; set; }
    }
}