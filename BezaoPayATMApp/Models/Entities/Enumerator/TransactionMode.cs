﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BezaoPayATMAppDAL.Models.Entities.Enumerator
{
    public enum TransactionMode
    {
        Debit,
        Credit,
    }
}
