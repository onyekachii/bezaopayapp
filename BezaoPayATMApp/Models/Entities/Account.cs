﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace BezaoPayATMAppDAL.Models.Entities
{
    public class Account
    {
        public int Id { get; set; }

        [MaxLength(10)]
        [MinLength(10)]
        public double AccountNumber { get; set; }

        [Column(TypeName = "decimal(38,2)")]
        public decimal Balance { get; set; }

        public DateTime Created { get; set; }
    }
}
