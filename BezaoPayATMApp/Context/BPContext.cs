﻿using BezaoPayATMAppDAL.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;

namespace BezaoPayATMAppDAL.Context
{
    public class BPContext : DbContext
    {        
        public BPContext()
        {
            
        }
        public BPContext(DbContextOptions<BPContext> options) : base(options)
        {
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Data Source=(LocalDB)\MSSQLLocalDB;
				 Initial Catalog=CFBEZAOPay; Integrated Security=True;Connect Timeout=30;");
            base.OnConfiguring(optionsBuilder);
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
    }
}