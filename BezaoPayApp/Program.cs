﻿using System;
using BezaoPayATMAppDAL.Context;
using BezaoPayATMAppDAL.Models.Entities;
using Microsoft.EntityFrameworkCore.Design;

namespace BezaoPayApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Application.Run();
        }
    }
}
