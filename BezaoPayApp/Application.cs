﻿using BezaoPayApp.Utilities.Enumerators;
using BezaoPayATMAppDAL.Context;
using BezaoPayATMAppDAL.Models.BezaoPayServiceModels;
using BezaoPayATMAppDAL.Repository.Services;
using BezaoPayATMAppDAL.UnitWork;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace BezaoPayApp
{
    static class Application
    {
        static ConsoleColor DarkCyan = ConsoleColor.DarkCyan;
        static string SelectedLanguageIndex;
        public static LanguageOptions selectedLanguage;

        static LanguageOptions LanguageSelection(string LanguageSelectVariable)
        {
            try
            {
                switch (LanguageSelectVariable)
                {
                    case "0":
                        Console.WriteLine("\nPress \n1: Create Account \n2: Login");
                        return LanguageOptions.English;
                    case "1":
                        Console.WriteLine("\nlatsa \n1: Kirkira ajiya \n2: Shiga");
                        return LanguageOptions.Hausa;
                    case "2":
                        Console.WriteLine("\nTinye \n1: mebe Akauntu \n2: Banye Akauntu");
                        return LanguageOptions.Igbo;
                    case "3":
                        Console.WriteLine("\nJo te \n1: Loyun \n2. E wole");
                        return LanguageOptions.Yoruba;
                    default:
                        Run();
                        return LanguageOptions.NoSelection;
                }
            }
            catch
            {
                Run();
                return LanguageOptions.NoSelection;
            }
            
        }

        static void ServiceAction(string serviceAction, LanguageOptions selectedLanguage)
        {
            try
            {
                switch (serviceAction)
                {
                    case "1":
                        CreateAccountAsync(selectedLanguage);
                        break;
                    case "2":
                        try
                        {
                            Login();
                        }
                        catch(Exception e)
                        {
                            Console.WriteLine(e.Message);
                            Login();
                        }
                        break;
                    default:
                        LanguageSelection(SelectedLanguageIndex);
                        break;
                }
            }
            catch
            {
                Run();
            }
        }

        static async void CreateAccountAsync(LanguageOptions selectedLanguage)
        {
            bool Iserror = false;
            var newUser = new RegisterViewModel();
            Console.WriteLine("Enter your first name");
            newUser.FirstName = Console.ReadLine();
            Console.WriteLine("Enter your last name");
            newUser.LastName = Console.ReadLine();
            Console.WriteLine("Enter your Email");
            newUser.Email = Console.ReadLine();
            Console.WriteLine("Enter your username");
            newUser.Username = Console.ReadLine();
            Console.WriteLine("Enter your PIN");
            newUser.Passcode = Console.ReadLine();
            Console.WriteLine("Confirm PIN");
            newUser.ConfirmPasscode = Console.ReadLine();
            restartBirthdayInputs:
            Console.WriteLine("\nEnter Month of birth: Range of 01 - 12");
            var bMonth = Console.ReadLine();
            Console.WriteLine("Enter Day of birth: Range of 01 - 31");
            var bDay = Console.ReadLine();
            Console.WriteLine("Enter Year of birth");
            var bYear = Console.ReadLine();
            var birthday = @$"{bMonth}/{bDay}/{bYear}";
            try
            {
                newUser.Birthday = DateTime.Parse(birthday);
            }
            catch (ArgumentNullException e)
            {
                Iserror = true;
                Console.WriteLine(e.Message);
                Console.WriteLine("Empty/null value is prohibited in Date of Birth inputs.");
            }
            catch (FormatException e)
            {
                Iserror = true;
                Console.WriteLine(e.Message);
                Console.WriteLine("Please follow specified formats for your date of birth inputs");
            }
            catch (Exception e)
            {
                Iserror = true;
                Console.WriteLine(e.Message);
            }
            if (Iserror)
                goto restartBirthdayInputs;
            var register = new BezaoPayUserService(new UnitOfWork());
            await register.RegisterAsync(newUser);
            Run();
        }

        static void Login()
        {            
            var login = new LoginViewModel();
            Console.WriteLine("Enter your username or email");
            login.Username = Console.ReadLine();
            Console.WriteLine("Enter your PIN");
            login.Passcode = Console.ReadLine();
            new BezaoPayUserService(new UnitOfWork(new BPContext())).Login(login);
        }

        static void TransactionAction(string transactionAction)
        {
            var transaction = new Transaction();
            switch (transactionAction)
            {
                case "1":
                    transaction.UpdateProfile();
                    break;
            }
        }


        static public void Run()
        {
            try
            {
                Console.ForegroundColor = DarkCyan;
                var welcomeMenu = new StringBuilder();
                welcomeMenu.AppendLine("Welcome To BezaoPay ATM APP\n Please select your prefered language:");
                welcomeMenu.AppendLine(" 0: English\n 1: Hausa\n 2: Igbo\n 3: Yoruba");
                Console.WriteLine(welcomeMenu.ToString());
                

                SelectedLanguageIndex = Console.ReadLine();
                welcomeMenu.Clear();

                selectedLanguage = LanguageSelection(SelectedLanguageIndex);

                var serviceAction = Console.ReadLine();
                ServiceAction(serviceAction, selectedLanguage);

                Console.WriteLine("Welcome to BezaoPay ATM App\nHow may I help you?");
                Console.WriteLine("1. Edit Profile \t2. Check Balance \t3. Withdraw \t4. Transfer \tPress any other key to end transaction");
                var transactionAction = Console.ReadLine();
                TransactionAction(transactionAction);
            }
            catch
            {
                Run();
            }
        }
    }
}
