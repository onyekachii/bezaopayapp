﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BezaoPayApp.Utilities.Enumerators
{
    enum LanguageOptions
    {
        English,
        Yoruba,
        Igbo,
        Hausa,
        NoSelection
    }
}
